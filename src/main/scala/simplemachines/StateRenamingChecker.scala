package simplemachines

object StateRenamingChecker extends FsmEquivalenceChecker[FSMState] {

	/**
		* Checks whether the given finite state machines differ only by a renaming of their states.
		*
		* Machines using different alphabets are considered different even if the different symbols are essentially ignored.
		*
		*/
	override def isEquivalent(fsm1: BaseFiniteStateMachine[FSMState], fsm2: BaseFiniteStateMachine[FSMState]): Boolean =
		fsm1.alphabet == fsm2.alphabet && findRenaming(fsm1,fsm2).nonEmpty

	/**
		* Finds a renaming for the states of the given finite state machines.
		*
		* It is assumed that both finite state machines take the same input symbols.
		*
		* Notice that the renaming is unique if there is any.
		*
		* @return renaming mapping the states of fsm1 to the states of fsm2. In case there is no renaming [[Map.empty]]
		*         is returned.
		*/
	def findRenaming(fsm1: BaseFiniteStateMachine[FSMState], fsm2: BaseFiniteStateMachine[FSMState]): Map[FSMState,FSMState] =  {
		/**
			* Checks recursively whether there is a non-contradicting renaming.
			*
			* @param state1 state of fsm1 to be checked.
			* @param state2 state of fsm2 to be checked.
			* @param renaming renaming found already in the recursion, must be non-contradicting. For any contained
			*                 key-value pair the recursion is not progressed further
			* @return non-contradicting renaming if found, [[Map.empty]] if non could be found.
			*/
		def findRenamingRec(state1 : FSMState, state2 : FSMState, renaming : Map[FSMState,FSMState]) : Map[FSMState,FSMState] = {
			val consistent =
				if (renaming.contains(state1))
					renaming(state1) == state2  && (fsm1.isFinalState(state1) == fsm2.isFinalState(state2))
				else
					!renaming.values.toStream.contains(state2) // the map cannot be 1-to-1

			if (!consistent)
				Map.empty
			else if (renaming.contains(state1))
				renaming
			else {
				val transitionPairs: List[(FSMState, FSMState)] = for (symbol <- fsm1.alphabet.toList) yield
					(fsm1.process(state1, symbol), fsm2.process(state2, symbol))
				transitionPairs.foldLeft(renaming + {
					(state1, state2)
				}) { (currentRenaming, statePair) =>
					if (currentRenaming.isEmpty) Map.empty else findRenamingRec(statePair._1, statePair._2, currentRenaming)
				}
			}
		}

		findRenamingRec(fsm1.initialState, fsm2.initialState, Map.empty)
	}


}
