package simplemachines

/**
	* Models the state of a finite state machine, FSM for short.
	*
	* There are no mapping methods defined for states or symbols, because this would force any implementation to be
	* generic. Respective methods should be provided by implementing classes.
	* @tparam S states of the FSM.
	* */
trait BaseFiniteStateMachine[S] {

	/**
		* Returns true if and only if the given input symbols is accepted by the FMS, i.e., if it  the
		* initial to a final state when processed.
		*
		* @param inputSymbols input symbols to be processed and checked.
		*/
	def accepts(inputSymbols: Seq[Symbol]): Boolean

	/** Performs the transition from the given current state and the given input symbol.
		*
		* @param state       state of the FSM before the transition.
		* @param inputSymbol input symbol that triggers the transition.
		* @return new state of the machine after the transition.
		*/
	def process(state: S, inputSymbol: Symbol): S

	/**
		* Convenience method that applies [[process()]] multiple times.
		* The order of application is from first (left) to last (right), as in foldLeft.
		*
		* @param state        state of the FSM before the transition.
		* @param inputSymbols input symbols that triggers the transition.
		* @return new state of the machine after the transition.
		*/
	def process(state: S, inputSymbols: Seq[Symbol]): S

	/** returns the initial state for the FSM. */
	def initialState: S

	/** returns true if and only if the given state is a final state. */
	def isFinalState(state: S): Boolean

	/** returns the alphabet used by the FSM. */
	def alphabet : Set[Symbol]
}


