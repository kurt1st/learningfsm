package simplemachines

/**
	* Basic interface to check [[BaseFiniteStateMachine]]s for some equivalence relation.
	*/
trait FsmEquivalenceChecker[T] {
	def isEquivalent(fsm1: BaseFiniteStateMachine[T], fsm2: BaseFiniteStateMachine[T]) : Boolean
}


