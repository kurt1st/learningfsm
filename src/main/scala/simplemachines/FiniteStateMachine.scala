package simplemachines

import scala.util.{Failure, Success, Try}

/** This class implements a basic finite state machine, FSM for short.
	*
	* @param transitions  transitions of the FMS as a map, the default is that a state does not change for an incoming
	*                     symbol
	* @param initialState initial state of the FSM
	* @param finalStates  set of final states
	*/
class FiniteStateMachine(
	override val alphabet: Set[Symbol],
	val transitions: Map[(FSMState, Symbol), FSMState],
	override val initialState: FSMState,
	val finalStates: Set[FSMState]
) extends BaseFiniteStateMachine[FSMState] {

	private lazy val allStates: Set[FSMState] = transitions.values.toSet ++ finalStates + initialState

	override def accepts(inputSymbols: Seq[Symbol]): Boolean = {
		val stateAfterProcessing = process(initialState, inputSymbols)
		isFinalState(stateAfterProcessing)
	}

	/**
		* Performs the transition from the given current state and the given input symbol.
		*
		* @param state       state of the FSM before the transition.
		* @param inputSymbol input symbol that triggers the transition.
		* @return new state of the machine after the transition, if no particular transition is given the input state is
		*         returned as a default (no transition).
		*/
	override def process(state: FSMState, inputSymbol: Symbol): FSMState =
		transitions.getOrElse((state, inputSymbol), state)

	override def process(state: FSMState, inputSymbols: Seq[Symbol]): FSMState =
		if (inputSymbols.isEmpty)
			state
		else
			process(process(state, inputSymbols.head), inputSymbols.tail)

	/** returns true if and only if the given state is a final state. */
	override def isFinalState(state: FSMState): Boolean = finalStates.contains(state)

	/** returns this [[BaseFiniteStateMachine]] but with the given initial state. */
	def changeInitialState(newState: FSMState): FiniteStateMachine =
		new FiniteStateMachine(alphabet, transitions, newState, finalStates)

	/** returns this [[BaseFiniteStateMachine]] but with the given final states. */
	def changeFinalStates(newFinalStates: Set[FSMState]) =
		new FiniteStateMachine(alphabet, transitions, initialState, newFinalStates)

	/** return this [[FiniteStateMachine]] but with the given state marked as final. */
	def addFinalState(newFinalState: FSMState): FiniteStateMachine =
		new FiniteStateMachine(alphabet, transitions, initialState, finalStates + newFinalState)

	/**
		* This method returns this [[BaseFiniteStateMachine]] but with the given modified transitions.
		*
		* If the FSM is in the sourceState and the inputSymbol is processed then the next state after processing is the
		* targetState.
		*/
	def changeTransition(sourceState: FSMState, inputSymbol: Symbol, targetState: FSMState): FiniteStateMachine = {
		val newAlphabet = alphabet + inputSymbol
		val newTransitions: Map[(FSMState, Symbol), FSMState] = transitions.updated((sourceState, inputSymbol),
			targetState)
		new FiniteStateMachine(newAlphabet, newTransitions, initialState, finalStates)
	}

	/**
		* Returns the pullback FSM with respect to the given map.
		*
		* If the map does not conform with the transitions of the FSM, an [[UnsupportedOperationException]] is returned.
		* This cannot happen for a map that is 1-to-1 on the states.
		*
		* @param stateMap maps the current to the new states.
		* @return the mapped FSM or a Failure with an [[UnsupportedOperationException]].
		*/
	def mapStates(stateMap: Map[FSMState, FSMState]): Try[FiniteStateMachine] = {
		if (isValidStateMap(stateMap)) {
			val newTransitions: Map[(FSMState, Symbol), FSMState] =
				transitions.map { case ((source, symbol), target) =>
					((stateMap(source), symbol), stateMap(target))
				}
			val newFinalStates = finalStates.map(stateMap)
			Success(new FiniteStateMachine(alphabet, newTransitions, stateMap(initialState), newFinalStates))
		} else Failure(new UnsupportedOperationException("map does not conform with transitions"))
	}

	/** Returns true if and only if the map is a valid transformation of the states of the FSM. */
	private def isValidStateMap(map: Map[FSMState, FSMState]): Boolean = {
		val statePairs = allStates zip allStates

		statePairs.forall { case (s1, s2) =>
			if (s1 == s2 || map(s1) == map(s2))
				true
			else
				alphabet.forall(symbol =>
					process(s1, symbol) == process(s2, symbol)
				)
		}
	}

	/** Set of all states than can be reached via [[process()]] from the initial state. */
	lazy val reachableStates: Set[FSMState] = collectStates(initialState, Set.empty)

	private def collectStates(fromState: FSMState, alreadyCollected: Set[FSMState]): Set[FSMState] = {
		if (alreadyCollected.contains(fromState))
			alreadyCollected + fromState
		else {
			val nextStates = for (symbol <- alphabet) yield process(fromState, symbol)
			nextStates.foldLeft(alreadyCollected + fromState) {
				(collected, state) => collectStates(state, collected)
			}
		}
	}

	override def toString: String = {
		val formattedTransitions: Iterable[String] = for {keyValue <- transitions} yield {
			val ((source, symbol), target) = keyValue
			formatTransition(source, symbol, target)
		}
		formattedTransitions.mkString("[", ", ", "]")
	}

	private def formatTransition(source: FSMState, symbol: Symbol, target: FSMState): String =
		s"${formatState(source)} -$symbol-> ${formatState(target)}"

	private def formatState(state: FSMState): String = {
		val initialMarker = if (state == initialState) "*" else ""
		val finalMarker = if (isFinalState(state)) "f" else ""
		initialMarker + state + finalMarker
	}
}


/** initial object [[FiniteStateMachine]].
	*
	* The initial state is 0, there is no final state, and there is no non-trivial transition, i.e., processing a symbol
	* does have no effect.
	*/
object FiniteStateMachine {

	/**
		* Returns an empty finite state machine.
		*
		* The machine has initial state is 0, no transitions and hence no alphabet and no final states.
		*/
	val empty: FiniteStateMachine = new FiniteStateMachine(
		alphabet = Set.empty, transitions = Map.empty, initialState = 0, finalStates = Set.empty
	)
	/** Returns a finite state machines that accepts nothing. */
	val acceptsNothing: FiniteStateMachine = empty
	val acceptsEverything = new FiniteStateMachine(
		alphabet = Set.empty, transitions = Map.empty, initialState = 0, finalStates = Set(0)
	)

	/** Default factory method to create an empty finite state machines (see [[empty]]). */
	def apply(): FiniteStateMachine = empty

	def unusedState(fsm: FiniteStateMachine): FSMState = {
		val sourceStates: Iterable[Int] = fsm.transitions.keys.map(_._1)
		val targetStates: Iterable[Int] = fsm.transitions.values
		val allStates = sourceStates ++ targetStates
		if (allStates.isEmpty) 1 else allStates.max + 1
	}
}
