package simplemachines

import java.io.File

import language.postfixOps
import com.github.tototoshi.csv.{CSVReader, CSVWriter}


/**
	* Writes a [[BaseFiniteStateMachine]] to a file.
	*
	* The file is supposed to have no header and consists of 3 rows in the form
	* 	[source state], [input symbol], [target state],
	* where the source state is a non-negative integer, the target state is an integers, maybe negative, and the input
	* symbol is character, preferably an non-capitalized literal.
	*
	* The states of the finite state machine are given by positive integers. If source and target state are
	* non-negative integers the line defines a transition of the FSM. If the target state is a negative integer the
	* line does not define a transition and indicates that the source state is a final state. In this case the input
	* symbol is ignored. The initial state of the FSM is always given by 0
	*
	* The actual integers of the state of the FSM in the outputFile may differ from the integers of the input
	* [[BaseFiniteStateMachine]]. The finite state machines are only isomorphic up to reshuffling of the states.
	*/
object FsmWriter {

	/** type for transitions, (source, symbol, target). */
	protected type Transition = (FSMState,Char,FSMState)

	/**
		* Writes the given [[BaseFiniteStateMachine]] to a CSV file.
		*
		*
		* @param fsm finite state machine
		* @param outputFile output file
		*/
	def writeCsv(fsm : BaseFiniteStateMachine[FSMState], outputFile: File): Unit = {
		val transitions : Seq[Transition] = collectTransitions(fsm, List(fsm.initialState), Set.empty)
		val states : Set[FSMState] = (transitions.map(_._1) ++ transitions.map(_._3)) toSet

		val identityMap : Map[FSMState, Int] = Map().withDefault{state : FSMState => state}
		val renaming = identityMap.updated(fsm.initialState,0).updated(0, fsm.initialState)
		val renamedTransitions : Seq[Transition] =
			for( (source, symbol, target) <- transitions )
			yield (renaming(source),symbol, renaming(target))

		// add final states
		val finalTransitions : Seq[Transition] = for (s <- states.toSeq if fsm.isFinalState(s)) yield (renaming(s),
				' ',	-1)
		val allTransitions : Seq[Transition] = renamedTransitions ++ finalTransitions

		val writer = CSVWriter.open(outputFile)
		writer.writeAll( allTransitions.map( x => Seq(x._1, x._2, x._3)))
		writer.close()
	}

	/** Reads a [[BaseFiniteStateMachine]] from the given CSV file.
		*
		* @param inputFile input file
		* @return finite state machine
		*/
	def readCsv(inputFile : File) : BaseFiniteStateMachine[FSMState] = {
		val reader = CSVReader.open(inputFile)
		val fsm  = reader.toStream.foldLeft(FiniteStateMachine.empty){(fsm,line) =>
			val source : Int = line.head.toInt
			val symbol: Char = line(1).toStream.head
			val target : Int = line(2).toInt
			if (target < 0) fsm.addFinalState(source) else fsm.changeTransition(source, symbol, target)
		}
		reader.close()
		fsm
	}


	/**
		* Collects all transitions of the given finite state machine.
		*
		* To obtain all transitions of the FSM use statesToCollect=fsm.initialState and statesCollected=Set.empty.
		*
		* The returned transitions are complete in the following sense:
		* 1. For all states in statesToCollect all nontrivial transitions of the FSM from this state are returned.
		* 2. For all returned transitions, the target state is in statesCollected or all nontrivial transitions from the
		* target state are also returned.
		*
		* It is assumed that statesToCollect and statesCollected are disjoint.
		*
		* @param fsm	the finite state machine.
		* @param statesToCollect	states for which the transitions have to be collected at least.
		* @param statesCollected	states for which the the transitions have already been collected.
		*/
	// TODO This method should be private but that's so far too cumbersome for the tests.
	def collectTransitions(
													fsm:BaseFiniteStateMachine[FSMState],
													statesToCollect: List[FSMState],
													statesCollected:Set[FSMState]
												) : Seq[Transition] = statesToCollect match {
		case state :: rest =>
			val transitions: Set[(FSMState, Symbol, FSMState)] =
				for (symbol <- fsm.alphabet)
				yield (state, symbol, fsm.process(state,symbol))
			val reducedTransitions = transitions.filter{ t => t._1 != t._3 }

			val newStates = reducedTransitions.map{_._3}
				.filter{!statesToCollect.contains(_)}.filter{!statesCollected.contains(_)}

			reducedTransitions.toSeq ++ collectTransitions(fsm,rest ::: newStates.toList, statesCollected + state)
		case Nil => List.empty
	}
}
