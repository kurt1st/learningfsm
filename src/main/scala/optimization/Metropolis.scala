package optimization

import evaluators.FsmScorer
import generators.FsmTopology
import simplemachines.FiniteStateMachine

import scala.collection.GenSeq
import scala.util.Random

/**
	* Implements the metropolis algorithm.
	*
	* @param minimizer function to be minimized.
	* @param beta      inverse temperature, used in the algorithm to compute probabilities
	*/
class Metropolis(val minimizer: FsmScorer, val beta: Double) extends Optimizer {
	private val topology = FsmTopology

	/**
		* Finds a next approximation of the minimum from the given initial one.
		*
		* @return a [[FiniteStateMachine]] that is at least as good as the given one.
		*/
	override def improve(fsm: FiniteStateMachine): FiniteStateMachine = ???

	/**
		* Performs a single step of the metropolis algorithm.
		*
		* The returned finite state machine is random, hence results typically cannot be reproduced.
		*/
	def next(fsm: FiniteStateMachine): FiniteStateMachine = {
		val neighbours = topology.allNeighbours(fsm)
		val neighbourGibbs = for (n <- neighbours) yield (n, math.exp(-beta * minimizer.score(n)))
		val normalization: Double = neighbourGibbs.map(_._2).sum // sometimes called 'state sum'
		Metropolis.pickLabel(neighbourGibbs.toSeq, Random.nextDouble() * normalization)
	}

}

object Metropolis {
	/**
		* Picks the label selected by the given [[Double]] value.
		*
		* For this method we assume an interval [0, m) being partitioned into intervals [a,b) of a given length each
		* assigned with a label. The given parameter r then selects the interval, resp. its label,  to which the value
		* belong. If r is less than zero then it selects the first label. If it is larger then the maximal value m then it
		* selects the last label.
		*
		* @param labels sequence of labels and the length of the corresponding interval. Notice that the order matters
		*               here. It is assumed that the contained [[Double]] is positive. Must not be empty.
		* @param x      value that selects the label.
		* @return selected label
		*/
	private[optimization] def pickLabel[T](labels: GenSeq[(T, Double)], x: Double): T = {
		if (x < labels.head._2 || labels.tail.isEmpty)
			labels.head._1
		else
			pickLabel(labels.tail, x - labels.head._2)
	}
}


