package optimization

import evaluators.FsmScorer
import simplemachines.FiniteStateMachine

/**
	* All optimizers should implement this trait.
	*
	* Any [[Optimizer]] tries to find the best [[FiniteStateMachine]]. Here 'best' typically refers to a
	* maximal value with respect to a given [[FsmScorer]] provided with an implementation. More precisely, an
	* [[Optimizer]] tries to improve on a given [[FiniteStateMachine]].
	*
	* Most [[Optimizer]]s do not find the minimum but finish with an approximation, depending  on the used algorithm.
	*/
trait Optimizer {

	/**
		* Finds a next approximation of the minimum from the given initial one.
		*
		* @return a [[FiniteStateMachine]] that is at least as good as the given one.
		*/
	def improve(fsm: FiniteStateMachine): FiniteStateMachine
}
