package generators

import org.apache.commons.math3.distribution.PoissonDistribution
import simplemachines.FiniteStateMachine

import scala.util.{Failure, Random, Success, Try}

/**
	* Generates a random accepted word for a given finite state machine.
	*
	* To avoid endless loops only a fixed finite number of attempts to find an accepted word are executed. If no result
	* could be found, the result is a [[Failure]] with a [[NoAcceptedWordFound]] exception.
	*
	* @param fsm              finite state machine that accepts the generated words, must have nonempty alphabet
	* @param maxNumberOfTries maximal number of tries to generate a word
	* @param averageLength    average length of a word, must be strictly positive
	*
	*/
class WordGenerator( //
	private val fsm: FiniteStateMachine, //
	private val maxNumberOfTries: Int, //
	private val averageLength: Int //
) {
	require(averageLength > 0)
	require(fsm.alphabet.nonEmpty)

	/** distribution of the length of the word */
	val lengthDistribution = new PoissonDistribution(80)
	private val fsmSymbols = fsm.alphabet.toSeq

	/**
		* Returns the next random word.
		*
		* @return [[Success]] with the found word or [[Failure]] with a [[NoAcceptedWordFound]].
		*/
	def nextWord(): Try[Seq[Char]] = tryNextWord(maxNumberOfTries)

	private def tryNextWord(triesLeft: Int): Try[Seq[Char]] = {
		if (triesLeft <= 0)
			Failure(new NoAcceptedWordFound)
		else {
			val length = lengthDistribution.sample()
			val indices = Seq.fill(length)(Random.nextInt(fsmSymbols.size))
			val word = indices.map(k => fsmSymbols(k))
			if (fsm.accepts(word))
				Success(word)
			else
				tryNextWord(triesLeft - 1)
		}
	}


	/**
		* A stream of random words.
		*
		* The stream is infinite if there a next word can always be found (see [[nextWord()]]). Otherwise the stream is
		* finite
		*
		*/
	def wordStream(): Stream[Seq[Char]] = {
		val nextTry = nextWord()
		if (nextTry.isFailure)
			Stream.empty
		else
			nextTry.get #:: wordStream()
	}

	class NoAcceptedWordFound extends Exception

}

object WordGenerator {
	private val defaultMaxNumberOfTries = 100
	private val defaultAverageLength = 80

	def apply(fsm: FiniteStateMachine): WordGenerator =
		new WordGenerator(fsm, defaultMaxNumberOfTries, defaultAverageLength)
}
