package generators

import simplemachines.{FSMState, FiniteStateMachine}

import scala.collection.GenIterable

/**
	* Defines the topology among finite state machines.
	*
	* In fact we agree on the convention that '0' is the initial state of all finite state machines. Moreover we assume
	* a common fixed and set alphabet for all finite state machines, that is, [[FiniteStateMachine.alphabet]] is the
	* same set for all invoked machines.
	*
	* The topology is defined in terms of a neighbourhood relation. Two finite state machines are neighbours differ if
	* precisely one of the following conditions is satisfied:
	* 1. 	The set of final states differs in a single element, i.e., precisely one of the states is final for one machine
	* but not for the other.
	* 2.	A single transition differs, i.e., for precisely one state reachable in both machines and precisely one symbol
	* the final state of the transition in the machines differs.
	*
	* Notice that neighbouring finite state machines may differ in the set of reachable states due to a changed
	* transition.
	*/
object FsmTopology {

	/** Returns all neighbours of the given machine. */
	def allNeighbours(fsm: FiniteStateMachine): GenIterable[FiniteStateMachine] =
		neighboursByFinalState(fsm) ++ neighboursByTransition(fsm)

	/** Returns all neighbours that differ by a single final state. */
	private[generators] def neighboursByFinalState(fsm: FiniteStateMachine): GenIterable[FiniteStateMachine] = {
		val (finalStates, nonFinalStates) = fsm.reachableStates.partition(fsm.isFinalState)
		val addFinalStateMachines = for (s <- nonFinalStates) yield fsm.changeFinalStates(finalStates + s)
		val removeFinalStateMachines = for (s <- finalStates) yield fsm.changeFinalStates(finalStates - s)
		addFinalStateMachines ++ removeFinalStateMachines
	}

	/**
		* Returns all neighbours that differ by a single transition.
		*
		* This may introduce a transition to a new state.
		*/
	private[generators] def neighboursByTransition(fsm: FiniteStateMachine): GenIterable[FiniteStateMachine] = {
		for {
			fromState <- fsm.reachableStates
			symbol <- fsm.alphabet
			toState <- fsm.reachableStates + FiniteStateMachine.unusedState(fsm)
			if toState != fsm.process(fromState, symbol)
		} yield fsm.changeTransition(fromState, symbol, toState)
	}



	/** Returns true if and only if the given finite state machines are neighbours. */
	def areNeighbours(fsm1: FiniteStateMachine, fsm2: FiniteStateMachine): Boolean = {
	/* The simple implementation allNeighbours(fsm1).contains(fsm2) requires a solid implementation of equals() for FiniteStateMachines, which does not exist. Moreover, this implementation can be expected to be inefficient. */
		if (!fsm1.alphabet.equals(fsm2.alphabet))
			false
		else if (fsm1.initialState != fsm2.initialState)
			false
		else {
			val finalDifference = differenceOfFinalStates(fsm1, fsm2).size
			val transitionDifference = differenceOfTransitions(fsm1, fsm2).size
			if (finalDifference == 1 && transitionDifference == 0)
				true
			else if (finalDifference == 0 && transitionDifference == 1)
				true
			else
				false
		}
	}

	/** Returns the symmetric difference of the final states of the given [[simplemachines.FiniteStateMachine]]s. */
	private[generators] def differenceOfFinalStates(fsm1: FiniteStateMachine, fsm2: FiniteStateMachine) : Set[FSMState] =
		(fsm1.finalStates.diff(fsm2.finalStates)) union (fsm2.finalStates.diff(fsm1.finalStates))

	/** Returns the symmetric difference of the transitions of the given [[simplemachines.FiniteStateMachine]]s. */
	private[generators] def differenceOfTransitions(fsm1: FiniteStateMachine, fsm2: FiniteStateMachine) : Map[(FSMState, Symbol), FSMState] = ???

}
