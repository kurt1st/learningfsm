package generators

import simplemachines.{FSMState, FiniteStateMachine}

import scala.util.Random

/**
	* This object generates random finite state machines.
	*
	* It is intended to be used for a Monte Carlo Markov chain method. It generates a new random finite state machine by
	* modifying a given machine.
	*
	* The two given rates determine the likelihood at which the set of final final states is
	* modified and at which the transitions of the finite state machine is modified. More precisely, the respective
	* probabilities are given by normalizing the two rates.
	*
	* It is recommended to explicitly specify the alphabet of the finite state machine. Otherwise no transition can be
	* changed.
	*
	* @param finalStateChangeRate rate at which the set of final states is modified.
	* @param transitionChangeRate rate at which the transitions are changed.
	*/
class FsmGenerator(finalStateChangeRate: Float, transitionChangeRate: Float) {
	assert(finalStateChangeRate >= 0)
	assert(transitionChangeRate >= 0)


	/** Generates a new random finite state machine by modifying the given one. */
	def next(fsm: FiniteStateMachine): FiniteStateMachine = {
		if (fsm.alphabet.isEmpty //
			|| Random.nextFloat() * (finalStateChangeRate + transitionChangeRate) < finalStateChangeRate
		) modifyFinalStates(fsm) else modifyTransition(fsm)
	}

	private def modifyFinalStates(fsm: FiniteStateMachine): FiniteStateMachine = {
		val (finalStates, nonFinalStates) = fsm.reachableStates.partition(fsm.isFinalState)
		if (finalStates.isEmpty || (Random.nextBoolean() && nonFinalStates.nonEmpty)) { // add a final state
			val finalIndex = Random.nextInt(nonFinalStates.size)
			val newFinalState: FSMState = nonFinalStates.toSeq(finalIndex)
			fsm.changeFinalStates(finalStates + newFinalState)
		} else { // remove a final state
			val nonFinalIndex = Random.nextInt(finalStates.size)
			val newNonFinalState: FSMState = finalStates.toSeq(nonFinalIndex)
			fsm.changeFinalStates(finalStates - newNonFinalState)
		}
	}

	private def modifyTransition(fsm: FiniteStateMachine): FiniteStateMachine = {
		val fromState = randomEntryOf(fsm.reachableStates)
		val symbol = randomEntryOf(fsm.alphabet)
		val availableToStates = fsm.reachableStates + FiniteStateMachine.unusedState(fsm)
		val toState = randomEntryOf(availableToStates)
		fsm.changeTransition(fromState, symbol, toState)
	}

	private def randomEntryOf[T](collection: Iterable[T]): T =
		collection.toSeq.apply(Random.nextInt(collection.size))

}
