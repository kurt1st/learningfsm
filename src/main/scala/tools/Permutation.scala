package tools

import scala.language.postfixOps

/**
	* This trait represents a finite permutation on objects of the given generic type.
	*
	* Instances of [[Permutation]] are formed by composing [[Cycle]]s using [[DisjointUnion]], which are amenable to
	* pattern matching.
	*
	* @tparam T type for the objects of the permutation.
	*/
sealed trait Permutation[+T] {

	/** Returns the value after applying the permutation to the given object. */
	def apply[S >: T](element: S): S

	/** Returns the support of the permutation, i.e., the elements on which the permutation acts non-trivially. */
	def support : Iterable[T]

	/**
		* Composes with the given permutation.
		*
		* The other permutation
		* @param otherPermutation applied after this permutation
		*/
	def composeWith[S >: T](otherPermutation : Permutation[S]) : Permutation[S]

	def toMap[S >: T]: Map[S,S] = {
		val mapPairs = for ( x <- support ) yield {(x, apply(x))}
		Map[S,S]() ++ mapPairs	withDefault( x => x)
	}

	/** Returns the disjoint cycles that form the permutation. */
	def cycles[S >: T] : Iterable[Cycle[S]]
}

object Permutation {

	/** Return true if and only if the given [[tools.Permutation]] is equal to the identity. */
	//noinspection ComparingUnrelatedTypes
	def isIdentity( permutation : Permutation[Any]) : Boolean = permutation.cycles.isEmpty
}

/**
	* Identity permutation.
	*/
case class Identity() extends Permutation[Nothing] {
	override def apply[S](obj: S): S = obj
	override val support : Iterable[Nothing] =  Iterable.empty
	override def composeWith[S](otherPermutation: Permutation[S]): Permutation[S] = otherPermutation


	override def equals(obj : Any) : Boolean = {
		if (!obj.isInstanceOf[Permutation[Any]])
			false
		else
			Permutation.isIdentity(obj.asInstanceOf[Permutation[Any]])
	}

	override def cycles[S >: Nothing]: Iterable[Cycle[S]] = Iterable.empty
}

/**
	* Cyclic permutation.
	*
	* @param elements must have at least 2 elements, must be distinct.
	* @tparam T type for the objects of the permutation.
	*/
case class Cycle[+T] (elements: Vector[T])  extends Permutation[T] {
	require(elements.size >= 2)
	require(elements.distinct.size == elements.size)

	override def apply[S >: T](obj: S): S = {
		val index = elements.indexOf(obj) // -1 if not contained
		if (index < 0)
			obj
		else {
			val followingIndex = (index + 1) % elements.size
			elements(followingIndex)
		}
	}

	override val support : Iterable[T] = elements

	override def composeWith [S >: T] (otherPermutation : Permutation[S]) : Permutation[S] =  {
			val commonSupport : Set[S] = this.elements.toSet & otherPermutation.support.toSet
			if (commonSupport.isEmpty)
				// easy early return without computation
				DisjointUnion(this, otherPermutation)
			else { // recompute all cycles
				val jointSupport  = elements ++ otherPermutation.support
				val allCyclesSequences =
					for (x <- jointSupport) yield completeCycleOfComposition(otherPermutation, Vector(x))
				val allNonTrivialCycleSequences = allCyclesSequences.filter(_.size > 1)
				val cycles : Set[Permutation[S]] =
					allNonTrivialCycleSequences map( Cycle(_) ) toSet

				cycles.size match {
					case 0 => Identity()
					case 1 => Cycle(allNonTrivialCycleSequences.head)
					case _ => DisjointUnion(cycles)
				}
			}
	}

	/** Helper method for [[Cycle.composeWith()]]. */
	private def completeCycleOfComposition [S >: T] (otherPermutation : Permutation[S], cycleStart: Vector[S]) : Vector[S] = {
		val nextElement : S = otherPermutation.apply(this.apply(cycleStart.last))
		if (nextElement == cycleStart.head)
			cycleStart
		else
			completeCycleOfComposition(otherPermutation, cycleStart :+ nextElement)
	}

	override def equals(obj : Any) : Boolean = {
		if (!obj.isInstanceOf[Permutation[Any]])
			false
		else {
			val otherCycles = obj.asInstanceOf[Permutation[Any]].cycles
			if (otherCycles.size != 1)
				false
			else {
				val Cycle(otherElements) = otherCycles.head
				val shiftIndex = otherElements.indexOf(this.elements.head)
				if (shiftIndex < 0)
					false
				else {
					val (before, after) = otherElements.splitAt(shiftIndex)
					val shiftedOtherElements: Vector[Any] = after ++ before
					shiftedOtherElements == this.elements
				}
			}
		}
	}

	override def cycles [S >: T] : Iterable[Cycle[S]] = Iterable(this)
}

object Cycle {
	/** Convenience factory. */
	def apply[T](elements : T*) : Cycle[T] = Cycle(elements.toVector)
}

/**
	* Union of arbitrary many disjoint permutations.
	*
	* If the [[permutations]] are possibly not disjoint, you may compose them in order to obtain a valid [[Permutation]].
	*
	* @param permutations must be disjoint (unchecked),
	*                     may contain [[DisjointUnion()]]s itself.
	* @tparam T type for the elements of the permutation.
	*/
case class DisjointUnion[+T](permutations : Iterable[Permutation[T]]) extends Permutation[T] {

	// Since the permutations are disjoint, only one applies, the others act trivially.
	override def apply[S >: T](obj: S): S = permutations.foldLeft(obj) {case (x,perm) => perm(x)}

	override def support: Iterable[T] = permutations.foldLeft(Iterable[T]()){ case(support, perm) => support ++ perm.support }

	override def composeWith[S >: T](otherPermutation: Permutation[S]): Permutation[S] = {
		// split into disjoint support and "the reset", the disjoint permutations need not be modified
		val (disjointPerms, nonDisjointPerms) = permutations.partition { perm =>
			val commonSupport = perm.support.toSet & otherPermutation.support.toSet
			commonSupport.isEmpty
		}

		// recompute all non-disjoint cycles from scratch
		val jointSupport: Set[S] = otherPermutation.support.toSet ++ nonDisjointPerms.flatMap(_.support).toSet
		val allCyclesSequences =
			for (x <- jointSupport) yield completeCycleOfComposition(otherPermutation, Vector(x))
		val allNonTrivialCycleSequences = allCyclesSequences.filter(_.size > 1) // eliminate identity a.k.a. trivial cycle
		val newCycles: Set[Permutation[S]] = allNonTrivialCycleSequences map (Cycle(_)) toSet // eliminate duplicates of identical cycles

		// Simplify if no union is required
		val newPerms: Iterable[Permutation[S]] = disjointPerms ++ newCycles
		newPerms.size match {
			case 0 => Identity()
			case 1 => newPerms.head
			case _ => DisjointUnion(newPerms)
		}
	}


	/** Helper method for [[DisjointUnion.composeWith()]]. */
	private def completeCycleOfComposition[S >: T](otherPermutation: Permutation[S], cycleStart: Vector[S]): Vector[S] = {
		val nextElement: S = otherPermutation.apply(this.apply(cycleStart.last))
		if (nextElement == cycleStart.head)
			cycleStart
		else
			completeCycleOfComposition(otherPermutation, cycleStart :+ nextElement)
	}

	override def equals(obj : Any) : Boolean = {
		if (!obj.isInstanceOf[Permutation[Any]])
			false
		else {
			val thisCycles = this.cycles
			val otherCycles = obj.asInstanceOf[Permutation[Any]].cycles
			thisCycles == otherCycles.toSet
		}
	}

	override def toString: String = {
		val permutationStr = for (perm <- permutations) yield "\t"+perm.toString
		"DisjointUnion(\n" + permutationStr.reduce( (s1,s2) => s1 +",\n"+ s2 ) + ")"
	}

	/** Returns the disjoint cycles that form the permutation. */
	override def cycles [S >: T] : Set[Cycle[S]] = {
		permutations.flatMap(_.cycles).toSet
	}
}

object DisjointUnion {
	/** Convenience factory. */
	def apply[T](permutations: Permutation[T]*): DisjointUnion[T] = DisjointUnion(permutations.toIterable)

}
