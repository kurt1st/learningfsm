/**
 * This  package provides a home for various classes and objects that are recurrently used in this projects but
 * not provided by the standard Scala libraries.
 */
package tools;
