package evaluators

import simplemachines.FiniteStateMachine

/** Returns the number of states as score of the finite state machine. */
object NumberOfStates extends FsmScorer {
	override def score(fsm: FiniteStateMachine): Double = fsm.reachableStates.size
}
