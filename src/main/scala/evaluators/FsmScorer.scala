package evaluators

import simplemachines.FiniteStateMachine

/**
	* Base trait for all evaluators for finite state machines.
	*
	* The finite state machines are evaluated by assigning a scoring number to them, the higher the number the better
	* the machine.
	*/
trait FsmScorer {
	/** Returns the score assigned to the given finite state machine. */
	def score(fsm: FiniteStateMachine): Double
}
