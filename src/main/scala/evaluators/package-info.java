/**
 * This package contains the interface and various implementations of evaluators for finite state machines.
 */
package evaluators;