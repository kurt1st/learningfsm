package evaluators

import simplemachines.FiniteStateMachine

import scala.collection.GenTraversable

/**
	* The score of this evaluator is the ration of words that are accepted by the finite state machine.
	*
	* @param words collection of words the score is based on, must be [[GenTraversable.nonEmpty]].
	**/
class RatioOfAcceptance(words: GenTraversable[Seq[Char]]) extends FsmScorer {
	assert(words.nonEmpty)

	override def score(fsm: FiniteStateMachine): Double =
		words.count(w => fsm.accepts(w)).toFloat / words.size
}
