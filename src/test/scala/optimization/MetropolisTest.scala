package optimization

import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}

/** Tests for [[Metropolis]]. */
class MetropolisTest extends FlatSpec with PropertyChecks with Matchers {
	private val testLabels: Seq[(String, Double)] = Seq(("foo", 1), ("bar", 2), ("foobar", 1))

	"pickLabels" should "pick first for negative values" in {
		Metropolis.pickLabel(testLabels, -0.5) should be("foo")
	}

	it should "pick last for too large values" in {
		Metropolis.pickLabel(testLabels, 10) should be("foobar")
	}

	it should "pick correctly within range" in {
		Metropolis.pickLabel(testLabels, 0.9) should be("foo")
		Metropolis.pickLabel(testLabels, 1) should be("foo")
		Metropolis.pickLabel(testLabels, 2) should be("bar")
		Metropolis.pickLabel(testLabels, 3) should be("bar")
		Metropolis.pickLabel(testLabels, 3.5) should be("foobar")
		Metropolis.pickLabel(testLabels, 4) should be("foobar")
	}
}

