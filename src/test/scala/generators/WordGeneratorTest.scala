package generators

import org.scalatest.FlatSpec
import org.scalatest.prop.PropertyChecks
import simplemachines.FiniteStateMachine

import scala.util.Try

/** Collects tests for [[WordGenerator]]. */
class WordGeneratorTest extends FlatSpec with PropertyChecks {

	private val testFsm = FiniteStateMachine()
		.changeTransition(0, 'a', 1)
		.changeTransition(1, 'b', 0)
		.changeFinalStates(Set(1))

	private val sut = WordGenerator(testFsm)

	"generator" should "work without alphabet" in {
		val noAlphabet = FiniteStateMachine()
		val sut = WordGenerator(noAlphabet)
		assert(sut.nextWord().isFailure)
	}
	it should "fail if no word can be found" in {
		val noAcceptor = FiniteStateMachine()
			.changeTransition(0, 'a', 1)
			.changeFinalStates(Set(0))
		val noChanceSut = WordGenerator(noAcceptor)
		val word: Try[Seq[Char]] = noChanceSut.nextWord()
		assert(word.isFailure, s"word '$word' found")
	}

	"word" should "be found within number of tries" in {
		assert(sut.nextWord().isSuccess)
	}
	it should "be accepted" in {
		val word = sut.nextWord().getOrElse(Seq('a')) // 'a' is accepted, failure is tested elsewhere
		assert(testFsm.accepts(word))
	}
}
