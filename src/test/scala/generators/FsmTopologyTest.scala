package generators

import org.scalacheck.Arbitrary
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}
import simplemachines.{FiniteStateMachine, FiniteStateMachineTest}

/** Collects tests for [[FsmTopology]]. */
class FsmTopologyTest extends FlatSpec with PropertyChecks with Matchers {

	implicit private val arbitraryFSM: Arbitrary[FiniteStateMachine] = //
		Arbitrary(FiniteStateMachineTest.fsmGen)

	/** *0 --a--> 1f, 1f --b--> *0 */
	private val testFsm = FiniteStateMachine()
		.changeTransition(0, 'a', 1)
		.changeTransition(1, 'b', 0)
		.changeFinalStates(Set(1))

	private val sut = FsmTopology

	"neighbour by final state" should "have single modified final states" in {
		sut.neighboursByFinalState(testFsm).foreach { fsm =>
			if (fsm.isFinalState(1))
				fsm.isFinalState(0) should be(true)
			else
				fsm.isFinalState(0) should be(false)
		}
	}

	it should "have same transitions" in {
		for (fsm <- sut.neighboursByFinalState(testFsm)) {
			assert(fsm.transitions === testFsm.transitions)
		}
	}

	it should "have 2 of them" in {
		sut.neighboursByFinalState(testFsm) should have size 2
	}

	"neighbour by transition" should "have same final states" in {
		for (fsm <- sut.neighboursByTransition(testFsm)) {
			fsm.isFinalState(0) should be(false)
			fsm.isFinalState(1) should be(true)
		}
	}

	it should "have single modified transitions" in {
		for (fsm <- sut.neighboursByTransition(testFsm)) {
			val finalPairs = for (
				state <- Vector(0, 1);
				symbol <- testFsm.alphabet
			) yield (fsm.process(state, symbol), testFsm.process(state, symbol))
			val numberOfDifferences = finalPairs.count(pair => pair._1 != pair._2)
			assert(numberOfDifferences === 1, s"Wrong number of modified transitions for the FSM\n$fsm")
		}
	}

	it should "have 8 of them, one for each combination of state+symbol" in {
		sut.neighboursByTransition(testFsm) should have size 8
	}

	"differenceOfFinalStates" should "be empty for same FSM" in forAll{ fsm : FiniteStateMachine =>
		sut.differenceOfFinalStates(fsm, fsm) shouldBe empty
	}

	it should "be commutative" in forAll { (fsm1 : FiniteStateMachine, fsm2 : FiniteStateMachine) =>
		sut.differenceOfFinalStates(fsm1, fsm2) shouldEqual sut.differenceOfFinalStates(fsm2, fsm1)
	}

	it should "be 1 for final state neighbours" in forAll { fsm1: FiniteStateMachine =>
		for (fsm2 <- sut.neighboursByFinalState(fsm1)) {
			sut.differenceOfFinalStates(fsm1, fsm2) shouldEqual 1
		}
	}

	it should "be 0 for transition neighbours" in forAll { fsm1: FiniteStateMachine =>
		for (fsm2 <- sut.neighboursByTransition(fsm1)) {
			sut.differenceOfFinalStates(fsm1, fsm2) shouldEqual 0
		}
	}


	// Test: For fsm2 in sut.neightboursByTransition(fsm1) we should have differenceOfTransitions(fsm1,fsm2) = 1
	// Test: For fsm2 in sut.allNeighbours(fsm1) we should have areNeightbours(fsm1,fsm2) == true

	// Test: differenceOfTransitions(fsm1, fsm2) should be the same as differenceOfTransitions(fsm2, fsm1)

	// Test: differenceOfTransitions(fsm,fsm) is empty

}

