package generators

import org.scalacheck.Arbitrary
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}
import simplemachines.{FiniteStateMachine, FiniteStateMachineTest, StateRenamingChecker}

/** Collects tests for [[WordGenerator]]. */
class FsmGeneratorTest extends FlatSpec with PropertyChecks with Matchers {

	implicit private val arbitraryFSM: Arbitrary[FiniteStateMachine] = //
		Arbitrary(FiniteStateMachineTest.fsmGen)

	private val testFsm = FiniteStateMachine()
		.changeTransition(0, 'a', 1)
		.changeTransition(1, 'b', 0)
		.changeFinalStates(Set(1))

	"next FSM" should "be different" in {
		val sut = new FsmGenerator(1, 1)
				val nextFsm = sut.next(testFsm)
				assert(!StateRenamingChecker.isEquivalent(nextFsm, testFsm), s"\n$nextFsm\nwas equivalent to\n$testFsm")
	}

	"final states" should "should change" in {
		val sut = new FsmGenerator(1, 0)
		forAll { fsm: FiniteStateMachine =>
			val originalFinalStates = fsm.reachableStates.filter(fsm.isFinalState)
			val nextFsm = sut.next(fsm)
			val nextFinalStates = nextFsm.reachableStates.filter(nextFsm.isFinalState)
			val symmetricDifference = (originalFinalStates diff nextFinalStates) union (nextFinalStates diff
				originalFinalStates)
			symmetricDifference should have size 1
		}
	}

	"transitions" should "not change" in {
		val sut = new FsmGenerator(1, 0)
		val nextFsm = sut.next(testFsm)
		val originalFinalStates = testFsm.reachableStates.filter(testFsm.isFinalState)
		val nextFinalStates = nextFsm.reachableStates.filter(nextFsm.isFinalState)
		val symmetricDifference = (originalFinalStates diff nextFinalStates) union (nextFinalStates diff
			originalFinalStates)
		symmetricDifference should have size 1
	}

}
