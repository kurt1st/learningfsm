package simplemachines

import org.scalacheck.Arbitrary
import org.scalatest.FlatSpec
import org.scalatest.prop.PropertyChecks

/**
  * Collects tests of [[StateRenamingChecker]].
  *
  * [[StateRenamingChecker#isEquivalent]] can essentially be tested by [[StateRenamingChecker#findRenaming]]. For this reason the tests concentrate mainly on the later.
  */
class StateRenamingCheckerTest extends FlatSpec with PropertyChecks {

  implicit val arbitraryFSM : Arbitrary[FiniteStateMachine] = Arbitrary(FiniteStateMachineTest.fsmGen)

  "Same FSM" should "be equal" in forAll { fsm: FiniteStateMachine =>
    assert(StateRenamingChecker.isEquivalent(fsm,fsm))
  }

  it should "have a trivial renaming" in forAll { fsm: FiniteStateMachine =>
    val renaming = StateRenamingChecker.findRenaming(fsm,fsm)
    assert(renaming.forall{ case(state1, state2) => state1==state2})
  }

  "Chain FSM" should "have a renaming" in {
    val fsm1 = chainFsm(0,1,2)
    val fsm2 = chainFsm( 2,1,0)
    val renaming = StateRenamingChecker.findRenaming(fsm1, fsm2)
    for (s <- Range(0, 3))
      assert(renaming(s) === 2-s)
  }

  it should "have no renaming when changing initial state" in {
    val fsm = chainFsm(0,1,2)
    val modifiedFsm =fsm.changeInitialState(1)
    assert(StateRenamingChecker.findRenaming(fsm, modifiedFsm).isEmpty)
  }

  it should "have no renaming when changing final state" in {
    val fsm = chainFsm(0,1,2)
    val modifiedFsm =fsm.addFinalState(1)
    assert(StateRenamingChecker.findRenaming(fsm, modifiedFsm).isEmpty)
  }

  /**
    * Returns a chain FSM.
    *
    * If (s1,..., sN) are the given states, the returned FSM has transitions
    *   (s1) -a-> (s2) -a-> ... -a-> (sN)
    * for the input symbol 'a' and
    *   (s1) <-b- (s2) <-b- ... <-b- (sN)
    * for the input symbol 'b'. The first state given as parameter is the initial state, the last state is final.
    *
    * @param states states of the returned FSM, should be distinct.
    */

  // TODO This code should also be tested.
  private def chainFsm (states : FSMState*) : FiniteStateMachine = {
    val statePairs = for ( k <- Range(0,states.size-1) ) yield { (states(k), states(k+1))}
    val fsmWithTransitions = statePairs.foldLeft(FiniteStateMachine()){ (fsm, statePair) =>
      fsm.changeTransition(statePair._1, 'a', statePair._2)
        .changeTransition(statePair._2, 'b', statePair._1)
    }
    fsmWithTransitions.changeInitialState(states.head).changeFinalStates(Set(states.last))
  }

}
