package simplemachines


import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}
import tools.Permutation

import scala.util.Try

class FiniteStateMachineTest extends FlatSpec with PropertyChecks with Matchers {
	implicit private val arbitraryState: Arbitrary[FSMState] = //
		Arbitrary(FiniteStateMachineTest.stateGen)
	implicit private val arbitrarySymbol: Arbitrary[Symbol] = //
		Arbitrary(FiniteStateMachineTest.symbolGen)
	implicit private val arbitraryFSM: Arbitrary[FiniteStateMachine] = //
		Arbitrary(FiniteStateMachineTest.fsmGen)


	"Empty FSM" should "have no alphabet" in {
		assert(FiniteStateMachine.empty.alphabet.isEmpty)
	}

	it should "not process" in forAll { (state: FSMState, symbol: Symbol) =>
		assert(FiniteStateMachine.empty.process(state, symbol) === state)
	}

	it should "never be in a final state" in forAll { state: FSMState =>
		assert(!FiniteStateMachine.empty.isFinalState(state))
	}

	it should "have only initial state as reachable" in {
		assert(FiniteStateMachine.empty.reachableStates === Set(FiniteStateMachine.empty.initialState))
	}

	"Initial state" should "change" in
		forAll { (fsm: FiniteStateMachine, state: FSMState) =>
			val newFSM = fsm.changeInitialState(state)
			assert(newFSM.initialState === state)
		}


	"Final states" should "change" in
		forAll { (fsm: FiniteStateMachine, finalStates: Set[FSMState], state: FSMState) =>
			val newFsm = fsm.changeFinalStates(finalStates)
			assert(newFsm.isFinalState(state) === finalStates.contains(state))
		}

	it should "be added" in
		forAll { (fsm: FiniteStateMachine, finalState: FSMState) =>
			val newFsm = fsm.addFinalState(finalState)
			assert(newFsm.isFinalState(finalState))
		}

	"Transition" should "change" in
		forAll { (fsm: FiniteStateMachine, source: FSMState, symbol: Symbol, target: FSMState) =>
			val newFsm = fsm.changeTransition(source, symbol, target)
			assert(newFsm.process(source, symbol) === target)
		}

	it should "not change for others" in
		forAll { (
			fsm: FiniteStateMachine,
			source: FSMState, symbol: Symbol, target: FSMState,
			otherSource: FSMState, otherSymbol: Symbol
		) =>
			whenever(symbol != otherSymbol || source != otherSource) {
				val newFsm = fsm.changeTransition(source, symbol, target)
				assert(newFsm.process(otherSource, otherSymbol) === fsm.process(otherSource, otherSymbol))
			}
		}

	"Processing sequences" should "not change state for empty input" in
		forAll { (fsm: FiniteStateMachine, source: FSMState) =>
			assert(fsm.process(source, Vector.empty) === source)
		}
	it should "coincide for single input" in {
		forAll { (fsm: FiniteStateMachine, source: FSMState, input: Symbol) =>
			assert(fsm.process(source, input) == fsm.process(source, Vector(input)))
		}
	}
	it should "iterate for sequences" in
		forAll { (
			fsm: FiniteStateMachine, source: FSMState, inputs1: Vector[Symbol],
			inputs2: Vector[Symbol]
		) =>
			assert(fsm.process(fsm.process(source, inputs1), inputs2) === fsm.process(source, inputs1 ++
				inputs2))
		}

	"Mapping states" should "transform initial state" in
		forAll { (fsm: FiniteStateMachine, newInitial: FSMState) =>
			val renaming: Map[FSMState, FSMState] = Map(fsm.initialState -> newInitial, newInitial ->
				fsm.initialState).withDefault(x => x)
			val newFsm: Try[FiniteStateMachine] = fsm.mapStates(renaming)
			assert(newFsm.isSuccess, "map should not fail")
			assert(newFsm.get.initialState === newInitial, "wrong initial state after map")
		}

	it should "make states final" in
		forAll { (fsm: FiniteStateMachine, oldFinal: FSMState, newFinal: FSMState) =>
			whenever(fsm.isFinalState(oldFinal)) {
				val renaming: Map[FSMState, FSMState] = Map(oldFinal -> newFinal, newFinal -> oldFinal)
					.withDefault(x => x)
				val newFsm: Try[FiniteStateMachine] = fsm.mapStates(renaming)
				assert(newFsm.isSuccess, "map should not fail")
				assert(newFsm.get.isFinalState(newFinal), "state must be final")
			}
		}

	it should "make states non-final" in
		forAll { (fsm: FiniteStateMachine, oldFinal: FSMState, newFinal: FSMState) =>
			whenever(!fsm.isFinalState(oldFinal)) {
				val renaming: Map[FSMState, FSMState] = Map(oldFinal -> newFinal, newFinal -> oldFinal)
					.withDefault(x => x)
				val newFsm: Try[FiniteStateMachine] = fsm.mapStates(renaming)
				assert(newFsm.isSuccess, "map should not fail")
				assert(!newFsm.get.isFinalState(newFinal), "state must not be final")
			}
		}

	it should "not fail for permutations" in
		forAll(FiniteStateMachineTest.fsmGen, FiniteStateMachineTest.statePermGen) {
			(fsm: FiniteStateMachine, renaming: Permutation[FSMState]) =>
				val newFsm: Try[FiniteStateMachine] = fsm.mapStates(renaming.toMap[FSMState])
				assert(newFsm.isSuccess)
		}

	it should "change processing covariantly" in
		forAll(FiniteStateMachineTest.fsmGen, FiniteStateMachineTest.statePermGen) {
			(fsm: FiniteStateMachine, renaming: Permutation[FSMState]) =>
				val newFsm: Try[FiniteStateMachine] = fsm.mapStates(renaming.toMap[FSMState])
				for {
					state <- FiniteStateMachineTest.usedStates
					symbol <- FiniteStateMachineTest.usedSymbols
				} {
					assert(newFsm.get.process(renaming.apply(state), symbol) === renaming.apply(fsm.process
					(state, symbol)))
				}
		}

	"accepts()" should "match with other methods" in
		forAll { (fsm: FiniteStateMachine, inputs: Vector[Symbol]) =>
			val processedState = fsm.process(fsm.initialState, inputs)
			assert(fsm.accepts(inputs) === fsm.isFinalState(processedState))
		}

	"reachable states" should "contain the initial state" in
		forAll((fsm: FiniteStateMachine) => assert(fsm.reachableStates.contains(fsm.initialState)))

	it should "be closed w.r.t. transitions" in
		forAll { fsm: FiniteStateMachine =>
			for {
				state <- fsm.reachableStates
				symbol <- fsm.alphabet
			} assert(fsm.reachableStates.contains(fsm.process(state, symbol)))
		}

	"new state" should "have no transitions" in
		forAll { (fsm: FiniteStateMachine, symbol: Symbol) =>
			val unusedState = FiniteStateMachine.unusedState(fsm)
			fsm.process(unusedState, symbol) should be(unusedState)
		}

	"acceptsNothing" should "accept nothing" in forAll { word: Seq[Symbol] =>
		FiniteStateMachine.acceptsNothing.accepts(word) should be(false)
	}

	"acceptsEverything" should "accept everything" in forAll { word: Seq[Symbol] =>
		FiniteStateMachine.acceptsEverything.accepts(word) should be(true)
	}
}

object FiniteStateMachineTest {

	val maxStates = 5
	val usedStates = Range(0, maxStates)
	val usedSymbols: Set[Symbol] = Set('a', 'b', 'c', 'd')

	/** default generator for [[FSMState]]s. */
	val stateGen: Gen[FSMState] = Gen.choose(min = 0, max = maxStates)

	/** default generator for [[Symbol]]s. */
	val symbolGen: Gen[Symbol] = Gen.oneOf(usedSymbols.toSeq)

	val statePermGen: Gen[Permutation[FSMState]] = tools.permutationGen(stateGen)


	/** default generator of finite state machines. */
	val fsmGen: Gen[FiniteStateMachine] = for {
		no_finalStates <- Gen.choose(min = 0, max = maxStates) // number of final states
		finalStates <- Gen.listOfN(no_finalStates, stateGen) // final states
		initialState <- stateGen // initial states
		transitionChain <- Gen.listOf(Gen.zip(symbolGen, stateGen)) // chains of transitions (symbol,
		// target)
	} yield {
		val sourceState: List[FSMState] = initialState :: transitionChain.map(x => x._2) dropRight 1
		val transitionList: List[(FSMState, (Symbol, FSMState))] = sourceState zip transitionChain
		val transitionMap: List[((FSMState, Symbol), FSMState)] =
			for (
				(source, (symbol, target)) <- transitionList
			) yield {
				((source, symbol), target)
			}
		val alphabet: Seq[Symbol] = for ((symbol, _) <- transitionChain) yield symbol
		new FiniteStateMachine(alphabet.toSet, transitionMap.toMap, initialState, finalStates.toSet)
	}

}