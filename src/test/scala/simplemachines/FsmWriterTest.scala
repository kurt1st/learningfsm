package simplemachines

import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, PrivateMethodTester}

class FsmWriterTest extends FlatSpec with PropertyChecks with PrivateMethodTester {


	private val fsmGen = FiniteStateMachineTest.fsmGen
	private implicit val arbitraryFSM : Arbitrary[FiniteStateMachine] = Arbitrary(fsmGen)
	private val someStateGen: Gen[Seq[FSMState]] = Gen.someOf(Range(0, FiniteStateMachineTest.maxStates))

	private val stubFsm = FiniteStateMachine()
		.changeTransition(0,'a', 1)
  	.changeTransition(1, 'a', 2)
  	.changeTransition(2,'b',1)
  	.changeTransition(1,'b',0)

	"No states" should "be collected" in {
		val transitions = FsmWriter.collectTransitions(stubFsm, List.empty, Set.empty)
		assert(transitions.isEmpty)
	}

	"All transitions of collect" should "be collected" in {
		val transitions = FsmWriter.collectTransitions(stubFsm, List(0,1), Set.empty)
		assert(transitions contains( (0,'a', 1) ))
		assert(transitions contains( (1,'a', 2) ))
		assert(transitions contains( (1,'b', 0) ))
	}

	"Returned transitions" should "be actual transitions" in
		forAll (fsmGen,someStateGen,someStateGen) { (fsm, statesToCollect, statesCollected) =>
			whenever(statesToCollect.forall(!statesCollected.contains(_))) {
				val transitions = FsmWriter.collectTransitions(fsm, statesToCollect.toList, statesCollected.toSet)
				for ((source, symbol, target) <- transitions) {
					assert(target === fsm.process(source, symbol))
				}
			}
		}

	it should "contain all stateToCollect" in
		forAll (fsmGen, someStateGen, someStateGen) { (fsm, statesToCollect, statesCollected) =>
			whenever(statesToCollect.forall(!statesCollected.contains(_))) {
				val transitions = FsmWriter.collectTransitions(fsm, statesToCollect.toList, statesCollected
					.toSet)
				for {
					state <- statesToCollect
					symbol <- FiniteStateMachineTest.usedSymbols if fsm.process(state, symbol) != state
				} {
					val target = fsm.process(state, symbol)
					assert(transitions contains( (state,symbol,target) ))
				}
			}
		}

	it should "be transitively complete" in
		forAll (fsmGen, someStateGen, someStateGen) { (fsm, statesToCollect, statesCollected) =>
			whenever (statesToCollect.forall(!statesCollected.contains(_))) {
				val transitions = FsmWriter.collectTransitions(fsm, statesToCollect.toList, statesCollected
					.toSet)
				for {
					(_, _, state) <- transitions if !statesCollected.contains(state)
					symbol <- FiniteStateMachineTest.usedSymbols if fsm.process(state, symbol) != state
				} {
					val target = fsm.process(state, symbol)
					assert(transitions contains( (state,symbol,target) ))
				}
			}
		}
}
