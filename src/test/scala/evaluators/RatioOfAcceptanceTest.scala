package evaluators

import org.scalacheck.Gen
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}
import simplemachines.FiniteStateMachine

/** Collects tests for [[evaluators.RatioOfAcceptance]]. */
class RatioOfAcceptanceTest extends FlatSpec with PropertyChecks with Matchers {

	"score" should "be zero if nothing matches" in {
		val neverAccepts = FiniteStateMachine().changeFinalStates(Set(1))
		forAll(Gen.nonEmptyListOf(Gen.nonEmptyListOf(Gen.alphaLowerChar))) {
			anyWords: Traversable[Seq[Char]] =>
				// by default initial state is 0 and no transitions
				val sut = new RatioOfAcceptance(anyWords)
				sut.score(neverAccepts) should be(0.0)
		}
	}

	it should "be 1 of everything matches" in {
		forAll(Gen.nonEmptyListOf(Gen.nonEmptyListOf(Gen.alphaLowerChar))) {
			val alwaysAccepts = FiniteStateMachine().changeFinalStates(Set(0))
			anyWords: Traversable[Seq[Char]] =>
				// by default initial state is 0 and no transitions
				val sut = new RatioOfAcceptance(anyWords)
				sut.score(alwaysAccepts) should be(1.0)
		}
	}

	it should "be 1/2 if half of the words match" in {
		val isNumberOfAsEven = FiniteStateMachine() //
			.changeTransition(0, 'a', 1) //
			.changeTransition(1, 'a', 0) //
			.changeFinalStates(Set(0))
		val words: Vector[Seq[Char]] = Vector("", "a", "aa", "aaa")
		val sut = new RatioOfAcceptance(words)
		sut.score(isNumberOfAsEven) should be(0.5)
	}

}
