package tools

import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.PropertyChecks

/**
	* Collects tests for the case classes of [[Permutation]].
	*
	* Test are group by the concerned case class. An exception is the method [[Permutation.composeWith()]],
	* which is tested in the devoted test class [[CompositionTest]].
	*/
class PermutationTest extends FlatSpec with PropertyChecks with Matchers {

	implicit private val arbCycle : Arbitrary[Cycle[Int]] = Arbitrary(cycleGen(Arbitrary.arbInt.arbitrary))

	"isIdentity" should "be true for Identity()" in {
		Permutation.isIdentity(Identity()) should be(true)
	}
	it should "be false for Cycles" in forAll { cycle : Cycle[Int] =>
		Permutation.isIdentity(cycle) should be(false)
	}
	it should "be true for empty DisjointUnion" in {
		Permutation.isIdentity(DisjointUnion()) should be(true)
		Permutation.isIdentity(DisjointUnion(Identity())) should be(true)
		Permutation.isIdentity(DisjointUnion(DisjointUnion(Identity()))) should be(true)
	}
	it should "be false for nontrivial DisjointUnion" in {
		Permutation.isIdentity(DisjointUnion(Cycle(1,2))) should be(false)
		Permutation.isIdentity(DisjointUnion(DisjointUnion(Cycle(1,2)))) should be(false)
		Permutation.isIdentity(DisjointUnion(Cycle(1,2), Cycle(3,4))) should be(false)
	}

	"Identity" should "have no cycles" in {
		Identity().cycles should be(empty)
	}

	"Cycle" should "cycle elements on apply" in {
		val perm = Cycle(1,2,3)
		assert( perm(1) === 2)
		assert( perm(2) === 3)
		assert( perm(3) === 1)
	}
	it should "apply trivially for non-cycle elements" in forAll { x: Int =>
		whenever(x < 1 || x > 3) {
			val perm: Permutation[Int] = Cycle(1, 2, 3)
			assert(perm(x) === x)
		}
	}
	it should "be supported on cycle elements" in forAll (Gen.choose(2,10)) { x : Int =>
		val elements = Range(0,x).toVector
		assert( Cycle(elements).support.toSet === elements.toSet )
	}
	it should "act non-trivial on support" in forAll { cycle : Cycle[Int] =>
		for (x <- cycle.support )	assert( cycle(x) != x )
	}
	it should "provide same values in toMap" in forAll { (perm: Cycle[Int], x : Int) =>
		assert( perm.apply(x) === perm.toMap[Int].apply(x) )
	}
	it should "be its own cycle" in forAll { cycle : Cycle[Int] =>
		cycle.cycles should( have size 1 and contain(cycle) )
	}

	"DisjointUnion" should "have defaults" in forAll { x: Int =>
		whenever(x < 1 || x > 4) {
			val perm = DisjointUnion(Cycle(1,2), Cycle(3,4))
			assert(perm.apply(x) === x)
		}
	}
	it should "give correct values" in {
		val perm = DisjointUnion(Cycle(1,2), Cycle(3,4))
		assert( perm(1) === 2 )
		assert( perm(2) === 1 )
		assert( perm(3) === 4 )
		assert( perm(4) === 3 )
	}
	it should "provide same values in toMap" in forAll { x : Int =>
		val perm = DisjointUnion(Cycle(1,2), Cycle(3,4))
		assert( perm.apply(x) === perm.toMap[Int].apply(x) )
	}
	it should "have correct support" in {
		val perm1 = Cycle(1,2,3,4)
		val perm2 = Cycle(6,7)
		val perm = DisjointUnion(perm1, perm2)
		assert( perm.support.toSet === Set(1,2,3,4, 6,7) )
	}
	it should "contain all cycles of a DisjointUnion" in {
		val cycle1 = Cycle(1,2)
		val cycle2 = Cycle(3,4)
		val cycle3 = Cycle(5,6)
		val cycles = DisjointUnion(cycle1, cycle2, cycle3).cycles
		cycles should have size 3
		cycles should contain allOf(cycle1, cycle2, cycle3)
	}
	it should "not care about nesting for cycles" in {
		val cycle1 = Cycle(1,2)
		val cycle2 = Cycle(3,4)
		val cycle3 = Cycle(5,6)
		val cycles = DisjointUnion(cycle1, DisjointUnion(cycle2, cycle3)).cycles
		cycles should have size 3
		cycles should contain allOf(cycle1, cycle2, cycle3)
	}
}
