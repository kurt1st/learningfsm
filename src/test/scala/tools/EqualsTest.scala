package tools

import org.scalacheck.Arbitrary
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}

/** Tests composition for the case classes of [[Permutation]]. */
//noinspection ComparingUnrelatedTypes
class EqualsTest extends FlatSpec with PropertyChecks with Matchers {

	implicit private val arbitraryPermutation: Arbitrary[Permutation[Int]] =
		Arbitrary(permutationGen(Arbitrary.arbInt.arbitrary))

	"Identity" should "be equal to itself" in {
		assert(Identity() equals Identity())
	}
	it should "not equal a Cycle" in {
		val id: Permutation[Int] = Identity() // without the explicit type we get a Scala warning
		assert(!(id equals Cycle(1, 2, 3)))
	}

	"Cycle" should "be the identity" in {
		Cycle(1, 2) should not equals Identity()
	}
	it should "not change for rotated elements" in {
		assert(Cycle(1, 2, 3) equals Cycle(2, 3, 1))
	}
	it should "differ for elements in different order" in {
		Cycle(1, 2, 3) should not equals Cycle(1, 3, 2)
	}
	it should "differ for different number of elements" in {
		assert {
			!(Cycle(1, 2) equals Cycle(1, 2, 3))
		}
	}

	"DisjointUnion" should "not change when Identity is added" in {
		assert(DisjointUnion(Cycle(1, 2), Cycle(3, 4)) === DisjointUnion(Cycle(1, 2), Cycle(3, 4),
			Identity()))
		assert(DisjointUnion(Cycle(1, 2), Cycle(3, 4)) === DisjointUnion(Identity(), Cycle(1, 2),
			Cycle(3, 4)))
		assert(DisjointUnion(Cycle(1, 2), Cycle(3, 4)) === DisjointUnion(Cycle(1, 2), Identity(),
			Cycle(3, 4)))
	}
	it should "not equal Identity" in {
		DisjointUnion(Cycle(1, 2)) should not equal Identity()
	}
	it should "not equals a Cycle" in {
		DisjointUnion(Cycle(1, 2), Cycle(3, 4)) should not equal Cycle(2, 3)
	}
	it should "delegate for a single permutation" in forAll { perm: Permutation[Int] =>
		DisjointUnion(Iterable(perm)) should equal(perm)
	}
	it should "should not change under reshuffling" in {
		DisjointUnion(Cycle(1, 2), Cycle(3, 4)) should equal(DisjointUnion(Cycle(3, 4), Cycle(1, 2)))
	}

	"Equals" must "be reflexive" in forAll { perm: Permutation[Int] =>
		perm should equal(perm)
	}
	it must "be symmetric" in forAll { (perm1: Permutation[Int], perm2: Permutation[Int]) =>
		val isEqual1 = perm1.equals(perm2)
		val isEqual2 = perm2.equals(perm1)
		isEqual1 should be(isEqual2)
	}
	// The condition for the generator is unlikely to be satisfied by arbitrary values
	it must "be transitive" in
		forAll { (perm1: Permutation[Int], perm2: Permutation[Int], perm3: Permutation[Int]) =>
			if (perm1.equals(perm2) && perm2.equals(perm3)) {
				/*
				This is unlikely to be satisfied by arbitrary values. But anyway a failure of this
				test---even if it is rare---indicates a wrong implementation.
				 */
				perm1 should equal(perm3)
			}
		}
}
