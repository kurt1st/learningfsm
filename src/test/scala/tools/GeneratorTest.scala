package tools

import org.scalacheck.Arbitrary
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}

/**
	* Tests the generators [[tools.permutationGen()]] and [[tools.cycleGen()]].
	*
	* The test typically have a small probability to fail even if the generator works fine.
	* */
class GeneratorTest extends FlatSpec with PropertyChecks with Matchers {

	private val permGen = permutationGen(Arbitrary.arbInt.arbitrary)

	"PermutationGen" should "provide an Identity" in {
		val onlyIdentity = permGen.retryUntil(_ == Identity())
		onlyIdentity.sample should not be None
	}

	it should "provide a Cycle" in {
		val onlyCycles = permGen retryUntil { _.isInstanceOf[Cycle[Int]] }
		onlyCycles.sample should not be None
	}

	it should "provide a DisjointUnion" in {
		val onlyCycles = permGen.retryUntil( _.cycles.size >= 2 )
		onlyCycles.sample should not be None
	}
}
