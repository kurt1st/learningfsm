
import org.scalacheck.{Arbitrary, Gen}

package object tools {
	// TODO implement some reasonable shrinking

	private val maxCycles = 3

	/** Generates a bijection from the given generator for single elements. */
	def permutationGen[T] (elementGen : Gen[T]) : Gen[Permutation[T]] = {
		val cycleListGen = Gen.listOfN(maxCycles, cycleGen(elementGen))
		val numberOfCyclesGen = Gen.chooseNum(0,maxCycles)
		for {
			n <- numberOfCyclesGen
			cycles <- cycleListGen
		} yield {
			cycles.take(n)
				.foldLeft[Permutation[T]] (Identity()) { case(c1, c2) => c1.composeWith(c2) }
		}
	}

	def cycleGen[T] (elementGen : Gen[T]) : Gen[Cycle[T]] =
		for {
			cycleElements <- Gen.nonEmptyContainerOf[Vector,T](elementGen)
			if cycleElements.distinct.size >=2
		} yield Cycle(cycleElements.distinct)

}
