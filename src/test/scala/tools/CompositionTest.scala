package tools

import org.scalacheck.Arbitrary
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers}

/** Tests composition for the case classes of [[Permutation]]. */
class CompositionTest extends FlatSpec with PropertyChecks with Matchers {

	implicit private val arbitraryPermutation : Arbitrary[Permutation[Int]] = Arbitrary(permutationGen(Arbitrary.arbInt.arbitrary))

	// Some time ago this test was failing very infrequently, but I couldn't pin down the reason.
	"Composition" should "be associative" in
		forAll { (perm1 : Permutation[Int], perm2 : Permutation[Int], perm3 : Permutation[Int]) =>
			val composition1 = perm1.composeWith(perm2).composeWith(perm3)
			val composition2 = perm1.composeWith(perm2.composeWith(perm3))
			composition1 should be (composition2)
		}

	"Identity" should "be an identity element for composition" in forAll { perm : Permutation[Int] =>
		Identity().composeWith(perm) should be (perm)
		perm.composeWith(Identity()) should be (perm)
	}

	it should "compose trivially with itself" in { // This test is doubled, but maybe we don't trust the generator
		Identity().composeWith(Identity()) should be (Identity())
	}
	it should "compose trivially with cycle" in {
		Identity().composeWith(Cycle(1,2,3)) should be (Cycle(1,2,3))
	}

	it should "compose trivially with DisjointUnion" in {
		val perm = DisjointUnion(Cycle(1,2), Cycle(3,4))
		Identity().composeWith(perm) should be (perm)
	}

	"Composing Cycle" should "yield a DisjointUnion if disjoint" in {
		val cycle1 = Cycle(1,2)
		val cycle2 = Cycle(3,4)
		cycle1.composeWith(cycle2) should be (DisjointUnion(cycle1, cycle2))
	}

	it should "yield Identity for inverses" in {
		val cycle1 = Cycle(1,2)
		val cycle2 = Cycle(1,2)
		cycle1.composeWith(cycle2) should be (Identity())
	}

	it should "enlarge cycles if not disjoint" in {
		val cycle1 = Cycle(1,2)
		val cycle2 = Cycle(3,1)
		// 1->2->2, 2->1->3, 3->3->1
		cycle1.composeWith(cycle2) should be (Cycle(1,2,3))
	}

	it should "join Cycles if necessary" in {
		assert ( Cycle(1,2,3).composeWith(Cycle(3,4)) === Cycle(1,2,4,3) )
	}

	it should "reduce the cycle if necessary" in {
		val cycle1 = Cycle(1,2,3,4)
		val cycle2 = Cycle(2,3)
		// 1->3, 2->3->2, 3->4, 4->1
		cycle1.composeWith(cycle2) should be (Cycle(1,3,4))
	}

	it should "add to Disjoint union" in {
		val cycle1 = Cycle(1,2)
		val cycle2 = Cycle(3,4)
		val cycle3 = Cycle(5,6)
		cycle1.composeWith(DisjointUnion(cycle2,cycle3)) should be (DisjointUnion(cycle1,cycle2,cycle3))
	}

	"DisjointUnion" should "add permutation if disjoint" in {
		val cycle1 = Cycle(1, 2)
		val cycle2 = Cycle(3, 4)
		val cycle3 = Cycle(5, 6)
		DisjointUnion(cycle1, cycle2).composeWith(cycle3) should be(DisjointUnion(cycle1, cycle2, cycle3))
	}

	"DisjointUnion" should "eliminate permutation if inverse" in {
		val cycle1 = Cycle(1, 2)
		val cycle2 = Cycle(3, 4)
		val cycle3 = Cycle(5, 6)
		DisjointUnion(cycle1, cycle2, cycle3).composeWith(cycle2) should be(DisjointUnion(cycle1, cycle3))
	}

	"DisjointUnion" should "merge cycles if necessary" in {
		val perm1 = DisjointUnion(Cycle(1, 2), Cycle(3, 4))
		val perm2 = Cycle(2, 3)
		// 1->2->3, 2->1, 3->4, 4->3->2 (1342)
		perm1.composeWith(perm2) should be(Cycle(1, 3, 4, 2))
	}

}
