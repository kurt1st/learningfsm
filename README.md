# LearningFsm

The goal of this project is to provide a tool for learning a finite state machine (short FSM) from log file or similar simple line-based file formats. The basic is idea is to identify simple recurring components of the line in order to separate trivial from non-trivial content.

## Tools and Standards
*[Scala](https://www.scala-lang.org/)* as the main programming language of this project. 

*[sbt](https://www.scala-sbt.org/)* as build tool for Scala. 

*[ScalaTest](http://www.scalatest.org/)* as testing framework.

## How to Build
To build the project simply start the sbt command line 
```
$ sbt
``` 
and run 
```
> compile
```
See also [sbt documentation](https://www.scala-sbt.org/1.x/docs/Running.html).

## How to Get Started
The IDEs *IntelliJ* and *Eclipse* both support sbt projects. For convenience you may easily import the projects. Up to now only IntelliJ is tested thoroughly. Please feel invited to try out Eclipse or other IDEs and update this document.

There is no executable for this project. To try out some functionality or to toy around, you may use scala scripts or a REPL. For both you may use your IDE. Alternatively you may run scrips as via
```
> scala FooScript.sc
```
or start the sbt REPL from the sbt command line via
```
> console
```
If you intend to frequently use the command line, please checkout the [sbt Reference Manual](https://www.scala-sbt.org/1.x/docs/index.html).

## Roadmap

### (DONE) Solid Base for Finite State Machines
In this step the basic functionality for finite state machines is provided. This includes
* reading from and writing to a files, e.g. CSV,
* verifying whether a given string is accepted by an FSM,
* generating a random string that is accepted by the FSM.


### Foundations for Learning Algorithm
In this step the learning problem is carefully modelled. The basic functionality to address the problem in the ensuing steps is provided. This will, for instance, comprise a suitable topology for finite state machines and / or some random generator for finite state machines.

ToDos
0. Currently reading a FSM from CSV is completely untested. Look for a suitable way to deal with resources (here file) in the test framework.
1.  Implement and test neighbour relation in FsmTopology.
2.  Implement and test Metropolis.
3.  Implement a prototypical search algorithm for finite state machines.
4.  Verify the algorithm with generated data.

### Implement the Adversarial Learning Algorithm


