name := "LearningFSM"

version := "0.1"

scalaVersion := "2.12.5"

// scala-csv : reading and writing csv files
libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.5"

// ScalaTest : testing framework including property based testing
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"

// commons-math3 for distributions
// https://mvnrepository.com/artifact/org.apache.commons/commons-math3
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.0"


